use std::net::{TcpListener, TcpStream, Shutdown};
use std::io::{Read, Write};
use std::time::Instant;

const SIZE: usize = 512*1024*1024;

fn handle_client(mut stream: TcpStream) {
    let mut data = vec!(1u8; SIZE);

    while match stream.read_exact(&mut *data) {
        Ok(_) => {
            // echo everything!
            let mut pos = 0;
            while pos < data.len() {
                let bytes_written = stream.write(&data[pos..]).unwrap();
                pos += bytes_written;
            }
            true
        },
        Err(_) => {
            println!("An error occurred, terminating connection with {}", stream.peer_addr().unwrap());
            stream.shutdown(Shutdown::Both).unwrap();
            false
        }
    } {}
}

fn main() {
    let listener = TcpListener::bind("0.0.0.0:3333").unwrap();
    // accept connections and process them, spawning a new thread for each one
    println!("Server listening on port 3333");
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr().unwrap());
                handle_client(stream);
            }
            Err(e) => {
                println!("Error: {}", e);
                /* connection failed */
            }
        }
    }
    // close the socket server
    drop(listener);
}
