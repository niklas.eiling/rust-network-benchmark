use std::net::{TcpStream};
use std::io::{Read, Write};
use std::time::Instant;

const SIZE: usize = 512*1024*1024;

fn calculate_bandwidth(total_time_in_ms: u128, iterations: u32, memsize: usize) -> f64 {
    let time_per_copy_in_ms = total_time_in_ms as f64 / iterations as f64;
    let bandwidth_in_bytes_per_s = (memsize * 1024) as f64 / time_per_copy_in_ms;
    bandwidth_in_bytes_per_s / (1024 * 1024) as f64
}


fn main() {
    match TcpStream::connect("localhost:3333") {
        Ok(mut stream) => {
            println!("Successfully connected to server in port 3333");

            let mut data = vec!(1u8; SIZE);
            for i in 0..SIZE {
                data[i as usize] = (i%256) as u8;
            }
            println!("start sending...");
            let mut pos = 0;
            let time_begin = Instant::now();
            while pos < data.len() {
                let bytes_written = stream.write(&data[pos..]).unwrap();
                pos += bytes_written;
            }
            let elapsed_time = time_begin.elapsed().as_millis();
            println!("Write BW: {} MiB/s", calculate_bandwidth(elapsed_time, 1, SIZE));


            //for i in 0..(SIZE/128) {
            //    stream.write(&data[(i*128) as usize..(i*128+128) as usize]).unwrap();
            //}
            println!("Sent Hello, awaiting reply...");

            let time_begin = Instant::now();
            match stream.read_exact(&mut *data) {
                Ok(_) => {
                    let elapsed_time = time_begin.elapsed().as_millis();
                    println!("Read BW: {} MiB/s", calculate_bandwidth(elapsed_time, 1, SIZE));
                    for i in 0..SIZE {
                        if data[i as usize] != (i%256) as u8 {
                            println!("Reply had errors!");
                            break;
                        }
                        if i == SIZE-1 {
                            println!("Reply was ok!");
                        }
                    }
                },
                Err(e) => {
                    println!("Failed to receive data: {}", e);
                }
            }
        },
        Err(e) => {
            println!("Failed to connect: {}", e);
        }
    }
    println!("Terminated.");
}
